package com.example.my4application;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Toast;

import com.example.my4application.api.ApiService;
import com.example.my4application.models.СashMachineModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.GsonBuilder;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    private static final int PERMISSION_REQUEST_CODE = 2356;
    private ApiService apiService;
    private List<СashMachineModel> listСashMachine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        createApiService();
        loadRates();
    }

    private void createApiService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://belarusbank.by/api/atm/")
                .client(createOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                        .create()))
                .build();
        apiService = retrofit.create(ApiService.class);
    }

    private void loadRates() {
        apiService.getCashMachine("Гомель").enqueue(new Callback<List<СashMachineModel>>() {
            @Override
            public void onResponse(@NotNull Call<List<СashMachineModel>> call, @NotNull Response<List<СashMachineModel>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    listСashMachine = new ArrayList<>();
                    listСashMachine.addAll(response.body());
                }
                getPermission();
            }

            @Override
            public void onFailure(@NotNull Call<List<СashMachineModel>> call, @NotNull Throwable t) {
                Toast.makeText(MapsActivity.this, "Error: " + t, Toast.LENGTH_LONG).show();
            }
        });
    }

    private OkHttpClient createOkHttpClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(getLoggingInterceptor())
                .build();
    }

    private HttpLoggingInterceptor getLoggingInterceptor() {
        if (BuildConfig.DEBUG) {
            return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(52.4345, 30.9754), 11));
        googleMap.setMyLocationEnabled(true);
        for (СashMachineModel cachMachine : listСashMachine) {
            LatLng coordinates = new LatLng(cachMachine.getGpsX(), cachMachine.getGpsY());
            googleMap.addMarker(new MarkerOptions().position(coordinates).title(cachMachine.getAddress() + cachMachine.getHouse()));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE) {
            getPermission();
        }
    }

    public void getPermission() {
        boolean permissionAccessCoarseLocationApproved =
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED;
        boolean permissionAccessFineLocationApproved =
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED;

        if (permissionAccessCoarseLocationApproved && permissionAccessFineLocationApproved) {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            if (mapFragment != null) {
                mapFragment.getMapAsync(this);
            }
        } else {
            ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_REQUEST_CODE);
        }
    }
}
