package com.example.my5application.mapper;

import com.example.my5application.models.BankDivisionsModel;
import com.example.my5application.models.InfokioskModel;
import com.example.my5application.models.PlaceBelarusbank;
import com.example.my5application.models.СashMachineModel;

import java.util.ArrayList;
import java.util.List;

public class MapperToPlaceBelarusbank {

    public static List<PlaceBelarusbank> convertCashMachineToPlace(List<СashMachineModel> cashMachineModels) {
        List<PlaceBelarusbank> belarusbankList = new ArrayList<>();

        for (СashMachineModel model : cashMachineModels) {
            belarusbankList.add(new PlaceBelarusbank(model.getAddress(), model.getHouse(),
                    model.getGpsX(), model.getGpsY()));
        }
        return belarusbankList;
    }

    public static List<PlaceBelarusbank> convertInfokioskToPlace(List<InfokioskModel> infokioskModels) {
        List<PlaceBelarusbank> belarusbankList = new ArrayList<>();

        for (InfokioskModel model : infokioskModels) {
            belarusbankList.add(new PlaceBelarusbank(model.getAddress(), model.getHouse(),
                    model.getGpsX(), model.getGpsY()));
        }
        return belarusbankList;
    }

    public static List<PlaceBelarusbank> convertBankDivisionsToPlace(List<BankDivisionsModel> bankDivisionsModels) {
        List<PlaceBelarusbank> belarusbankList = new ArrayList<>();

        for (BankDivisionsModel model : bankDivisionsModels) {
            belarusbankList.add(new PlaceBelarusbank(model.getStreet(), model.getHomeNumber(),
                    model.getGPSX(), model.getGPSY()));
        }
        return belarusbankList;
    }
}
