package com.example.my7application.data.meta.repository;

import com.example.my7application.data.models.Answer;
import com.example.my7application.data.models.Meta;
import com.google.gson.JsonObject;

import io.reactivex.Single;
import retrofit2.http.Body;

public interface FindingMetaRepository {
    Single<Meta> getMeta();

    Single<Answer> sendReply(@Body JsonObject bean);
}
