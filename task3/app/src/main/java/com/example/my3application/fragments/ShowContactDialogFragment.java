package com.example.my3application.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.my3application.interfasces.OnContactBDClickListener;
import com.example.my3application.room.Contact;

import java.util.ArrayList;
import java.util.List;

public class ShowContactDialogFragment extends AppCompatDialogFragment {
    private final static String KEY = "key";
    private List<Contact> contacts = new ArrayList<>();
    private OnContactBDClickListener contactDbClickListener;

    public static ShowContactDialogFragment newInstance(List<Contact> contacts) {
        Bundle args = new Bundle();
        args.putParcelableArrayList(KEY, (ArrayList<Contact>) contacts);
        ShowContactDialogFragment fragment = new ShowContactDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        contactDbClickListener = (OnContactBDClickListener) getActivity();
        if (getArguments() != null) {
            contacts = getArguments().getParcelableArrayList(KEY);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        CharSequence[] myArray = new CharSequence[contacts.size()];
        for (int i = 0; i < contacts.size(); i++) {
            myArray[i] = contacts.get(i).toString();
        }
        builder.setTitle("Выберите контакт")
                .setItems(myArray, new DialogInterface.OnClickListener() {
                    @SuppressLint("ResourceType")
                    public void onClick(DialogInterface dialog, int which) {
                        if (contactDbClickListener != null) {
                            contactDbClickListener.onContactDbClick(contacts.get(which));
                        }
                    }
                });
        return builder.create();
    }
}
