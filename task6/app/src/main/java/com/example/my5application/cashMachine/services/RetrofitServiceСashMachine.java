package com.example.my5application.cashMachine.services;

import com.google.gson.GsonBuilder;
import com.squareup.picasso.BuildConfig;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitServiceСashMachine {
    private static RetrofitServiceСashMachine retrofitServiceInstance;
    private static final String BASE_URL = "https://belarusbank.by/api/atm/";
    private Retrofit mRetrofit;

    private RetrofitServiceСashMachine() {
        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                        .create()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(createOkHttpClient())
                .build();
    }

    private OkHttpClient createOkHttpClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(getLoggingInterceptor())
                .build();
    }

    private HttpLoggingInterceptor getLoggingInterceptor() {
        if (BuildConfig.DEBUG) {
            return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE);
        }
    }

    public static RetrofitServiceСashMachine getInstance() {
        if (retrofitServiceInstance == null) {
            retrofitServiceInstance = new RetrofitServiceСashMachine();
        }
        return retrofitServiceInstance;
    }

    public Retrofit getRetrofit() {
        return mRetrofit;
    }
}
