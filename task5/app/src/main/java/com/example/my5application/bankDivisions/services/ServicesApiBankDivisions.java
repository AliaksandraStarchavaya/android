package com.example.my5application.bankDivisions.services;

import com.example.my5application.bankDivisions.services.interfaces.BankDivisionsApiService;

import retrofit2.Retrofit;

public class ServicesApiBankDivisions {
    private Retrofit retrofit;

    public ServicesApiBankDivisions() {
        this.retrofit = RetrofitServiceBankDivisions
                .getInstance()
                .getRetrofit();
    }

    public BankDivisionsApiService getApiService() {
        return retrofit.create(BankDivisionsApiService.class);
    }
}
