package com.example.my7application.data.models.rowModels;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.my7application.adapter.RecyclerViewAdapter;
import com.example.my7application.data.models.FieldsForm;
import com.example.my7application.factory.ViewHolderFactory;

public class NumericRowType implements RowType {
    private String tittle;
    private String name;

    public NumericRowType(String tittle, String name) {
        this.tittle = tittle;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getItemViewType() {
        return RowType.NUMERIC_ROW_TYPE;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder) {
        ViewHolderFactory.NumericViewHolder numericViewHolder = (ViewHolderFactory.NumericViewHolder) viewHolder;
        numericViewHolder.numericView.setText(tittle);


        RecyclerViewAdapter.listView.add(new FieldsForm(name, numericViewHolder.editNumeric));
    }

    @NonNull
    @Override
    public String toString() {
        return "NumericRowType{" +
                "tittle='" + tittle + '\'' +
                '}';
    }
}
