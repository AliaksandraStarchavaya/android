package com.example.my7application.di;

import com.example.my7application.MetaViewModel;

import dagger.Subcomponent;

@Subcomponent
public interface ViewModelSubComponent {

    @Subcomponent.Builder
    interface Builder {
        ViewModelSubComponent build();
    }

    void inject(MetaViewModel metaViewModel);
}
