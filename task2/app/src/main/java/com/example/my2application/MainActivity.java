package com.example.my2application;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;

import com.example.my2application.fragments.FirstFragment;
import com.example.my2application.fragments.TwoFragment;
import com.example.my2application.interfasces.OnRecyclerViewItemClick;

public class MainActivity extends AppCompatActivity implements OnRecyclerViewItemClick {

    public static final String KEYTEXTRECVIEW="KEY";
    private FragmentManager fragmentManager;
    private Fragment firstFragment;
    private Fragment twoFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (getIntent().getBooleanExtra("finish", false)) finish();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentManager = getSupportFragmentManager();
        addFragment();
    }

    private void addFragment() {
        /*
        firstFragment = fragmentManager.findFragmentByTag(FirstFragment.class.getName());
        if (firstFragment != null && firstFragment.isAdded()) {
            System.out.println("fragment added");
            return;
        }
         */
        firstFragment = FirstFragment.newInstance();
        fragmentManager
                .beginTransaction()
                .add(R.id.mainLayout, firstFragment, FirstFragment.class.getName())
                .addToBackStack(null)
                .commit();
        fragmentManager.executePendingTransactions();
    }

    private void addTwoFragment(Integer text) {
          /*
        twoFragment = fragmentManager.findFragmentByTag(TwoFragment.class.getName());
        if (twoFragment != null && twoFragment.isAdded()) {
            System.out.println("fragment added");
            return;
        }
         */
        twoFragment = TwoFragment.newInstance(text);
        fragmentManager
                .beginTransaction()
                .add(R.id.mainLayout, twoFragment, FirstFragment.class.getName())
                .addToBackStack(null)
                .commit();
        fragmentManager.executePendingTransactions();
    }

    @Override
    public void onClick(Integer text) {
       addTwoFragment(text);
    }
}
