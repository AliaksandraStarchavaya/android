package com.example.my7application.data.meta.services;

import retrofit2.Retrofit;

public class ServicesApiMeta {
    private Retrofit retrofit;

    public ServicesApiMeta() {
        this.retrofit = RetrofitServiceMeta
                .getInstance()
                .getRetrofit();
    }

    public MetaApiService getApiService() {
        return retrofit.create(MetaApiService.class);
    }
}
