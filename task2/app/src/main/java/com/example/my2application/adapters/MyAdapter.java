package com.example.my2application.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.my2application.R;
import com.example.my2application.interfasces.OnRecyclerViewItemClick;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private OnRecyclerViewItemClick mClickListener;
    private List<Integer> adapterDatas = new ArrayList<>();

    public void setmClickListener(OnRecyclerViewItemClick mClickListener) {
        this.mClickListener = mClickListener;
    }

    /*
            public MyAdapter(OnRecyclerViewItemClick click, List<Integer> myData) {
                mClickListener = click;
                adapterDatas.clear();
                adapterDatas.addAll(myData);
            }
   */
    public MyAdapter(List<Integer> myData) {
        adapterDatas.clear();
        adapterDatas.addAll(myData);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.fragment_recview, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.setText(adapterDatas.get(position));
    }

    @Override
    public int getItemCount() {
        return adapterDatas.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView textView1;
        private TextView textView2;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            textView1 = itemView.findViewById(R.id.text1RecView);
            textView2 = itemView.findViewById(R.id.text2RecView);
            itemView.setOnClickListener(v -> {
                if (mClickListener != null) {
                    mClickListener.onClick(getLayoutPosition());
                }
            });
        }

        void setText(Integer text) {
            textView1.setText("Title " + text);
            textView2.setText("Description " + text);
        }
    }
}
