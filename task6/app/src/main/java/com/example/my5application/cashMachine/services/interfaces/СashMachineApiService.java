package com.example.my5application.cashMachine.services.interfaces;

import com.example.my5application.models.СashMachineModel;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface СashMachineApiService {

    @GET("CashMachines")
    Single<List<СashMachineModel>> getCashMachine(@Query("city") String city);
}
