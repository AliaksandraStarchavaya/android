package com.example.my7application.data.meta.repository;


import com.example.my7application.data.meta.services.ServicesApiMeta;
import com.example.my7application.data.models.Answer;
import com.example.my7application.data.models.Meta;
import com.google.gson.JsonObject;

import io.reactivex.Single;

public class MetaRepository implements FindingMetaRepository {
    private ServicesApiMeta servicesApi;

    public MetaRepository(ServicesApiMeta servicesApi) {
        this.servicesApi = servicesApi;
    }

    @Override
    public Single<Meta> getMeta() {
        return servicesApi.getApiService().getMeta();
    }

    @Override
    public Single<Answer> sendReply(JsonObject bean) {
        return servicesApi.getApiService().sendReply(bean);
    }
}
