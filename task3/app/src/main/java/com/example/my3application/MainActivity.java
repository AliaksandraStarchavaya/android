package com.example.my3application;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Layout;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.my3application.fragments.ContactDisplayFragment;
import com.example.my3application.fragments.ShowContactDialogFragment;
import com.example.my3application.interfasces.OnContactBDClickListener;
import com.example.my3application.room.AppDatabase;
import com.example.my3application.room.Contact;
import com.example.my3application.room.ContactDaoContact;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class MainActivity extends AppCompatActivity implements OnContactBDClickListener {
    private final static String KEY = "KEY";
    private final static int REQUEST_CODE_PERMISSION_READ_CONTACTS = 657;
    private FragmentManager fragmentManager;
    private Button selectСontactBtn;
    private Button showContactsBtn;
    private Button showFromSPBtn;
    private Button showInNotificationBtn;
    private Fragment contactDisplayFragment;
    private AppDatabase database = App.getInstance().getDatabase();
    private ContactDaoContact dao = database.driverDao();
    private SharedPreferences sPrefNumberPhone;
    private View mainLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentManager = getSupportFragmentManager();
        findViews();
        setListeners();
    }

    private void findViews() {
        selectСontactBtn = findViewById(R.id.select_contact_btn);
        showContactsBtn = findViewById(R.id.show_contacts_btn);
        showFromSPBtn = findViewById(R.id.show_from_sp_btn);
        showInNotificationBtn = findViewById(R.id.show_in_notification_btn);
        mainLayout = findViewById(R.id.main_layout);
    }

    private void setListeners() {
        selectСontactBtn.setOnClickListener(v -> {
            if (checkPermission(Manifest.permission.READ_CONTACTS)) {
                openContacts();
            } else
                requestPermission(Manifest.permission.READ_CONTACTS, REQUEST_CODE_PERMISSION_READ_CONTACTS);
        });
        showContactsBtn.setOnClickListener(v -> {
            addShowContactsFragment();
        });
        showFromSPBtn.setOnClickListener(v -> {
            showFromSP();
        });
        showInNotificationBtn.setOnClickListener(v -> {
            showInNotification();
        });
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("1", name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void showInNotification() {
        String numberPhone = loadText();
        Runnable getByNumber = () -> {
            Contact contact = dao.getByNumber(numberPhone);
            System.out.println(contact);
            createNotificationChannel();
            Intent notificationIntent = new Intent(this, MainActivity.class);
            PendingIntent contentIntent = PendingIntent.getActivity(this,
                    0, notificationIntent,
                    PendingIntent.FLAG_CANCEL_CURRENT);
            NotificationCompat.Builder builder =
                    new NotificationCompat.Builder(this, "1")
                            .setSmallIcon(R.drawable.ic_launcher_background)
                            .setContentTitle("Сохраненный контакт")
                            .setContentText(contact.toString())
                            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                            .setContentIntent(contentIntent);
            NotificationManagerCompat notificationManager =
                    NotificationManagerCompat.from(this);
            notificationManager.notify(1, builder.build());
        };
        new Thread(getByNumber).start();
    }

    private void openContacts() {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        if (reqCode == 1) {
            Contact contact;
            Cursor cursor = this.getContentResolver().query(data.getData(), null, null, null, null);
            while (cursor.moveToNext()) {
                contact = new Contact();
                String id = cursor.getString(
                        cursor.getColumnIndex(
                                ContactsContract.Contacts._ID));
                Cursor curName = getContentResolver().query(ContactsContract.Data.CONTENT_URI, null,
                        ContactsContract.Data.MIMETYPE + " = ? AND " +
                                ContactsContract.CommonDataKinds.StructuredName.CONTACT_ID + " = " + id,
                        new String[]{ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE}, null);
                if (curName.moveToNext()) {
                    contact.family = curName.getString(curName.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME));
                    contact.name = curName.getString(curName.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME));
                }
                curName.close();
                Cursor curPhone = this.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                while (curPhone.moveToNext()) {
                    contact.phone = curPhone.getString(curPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                }
                curPhone.close();
                Cursor curEmail = this.getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{id}, null);
                while (curEmail.moveToNext()) {
                    contact.email = curEmail.getString(curEmail.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
                }
                curEmail.close();
                createContactInDb(contact);
            }
        }

    }

    public void createContactInDb(Contact contact) {
        Runnable insert = () -> {
            dao.insert(contact);
        };
        new Thread(insert).start();
        Toast.makeText(this, "Сохранение успешно!", Toast.LENGTH_LONG).show();
    }

    private void addShowContactsFragment() {
        Runnable read = () -> {
            fragmentManager
                    .beginTransaction()
                    .add(ShowContactDialogFragment.newInstance(dao.getAll()), null)
                    .commit();
        };
        new Thread(read).start();
    }

    @Override
    public void onContactDbClick(Contact contact) {
        contactDisplayFragment = ContactDisplayFragment.newInstance(contact);
        fragmentManager
                .beginTransaction()
                .replace(R.id.main_layout, contactDisplayFragment, ContactDisplayFragment.class.getName())
                .addToBackStack(null)
                .commit();
        fragmentManager.executePendingTransactions();
        saveText(contact.phone);
    }

    private boolean checkPermission(String permission) {
        return ContextCompat.checkSelfPermission(this, permission) == PERMISSION_GRANTED;
    }

    private void requestPermission(String permission, int requestCode) {
        ActivityCompat.requestPermissions(this, new String[]{permission},
                requestCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE_PERMISSION_READ_CONTACTS) {
            if (grantResults.length > 0 && grantResults[0] == PERMISSION_GRANTED) {
                openContacts();
            } else {
                Toast.makeText(this, permissions[0] + "Чтобы продолжить, необходимо разрешение для работы с контактами.", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void saveText(String text) {
        sPrefNumberPhone = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor ed = sPrefNumberPhone.edit();
        ed.putString(MainActivity.KEY, text);
        ed.apply();
    }

    private void showFromSP() {
        Snackbar.make(mainLayout, loadText(), Snackbar.LENGTH_LONG).show();
    }

    private String loadText() {
        sPrefNumberPhone = getPreferences(MODE_PRIVATE);
        return sPrefNumberPhone.getString(MainActivity.KEY, "");
    }
}
