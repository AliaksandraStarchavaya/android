package com.example.my5application.infokiosk.repository;

import com.example.my5application.infokiosk.services.ServicesApiInfokiosk;
import com.example.my5application.mapper.MapperToPlaceBelarusbank;
import com.example.my5application.models.PlaceBelarusbank;
import com.example.my5application.repository.FindingBankRepository;

import java.util.List;

import io.reactivex.Single;

public class InfokioskRepository implements FindingBankRepository {
    private ServicesApiInfokiosk servicesApi = new ServicesApiInfokiosk();

    @Override
    public Single<List<PlaceBelarusbank>> getPlaceBelarusbank(String city) {
        Single<List<PlaceBelarusbank>> map = servicesApi.getApiService().getInfokiosks(city)
                .map(MapperToPlaceBelarusbank::convertInfokioskToPlace);
        return map;
    }
}
