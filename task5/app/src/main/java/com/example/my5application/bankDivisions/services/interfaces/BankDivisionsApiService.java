package com.example.my5application.bankDivisions.services.interfaces;

import com.example.my5application.models.BankDivisionsModel;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface BankDivisionsApiService {

    @GET("BankDivisions")
    Single<List<BankDivisionsModel>> getBankDivisions(@Query("city") String city);
}
