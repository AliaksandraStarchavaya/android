package com.example.my2application.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.my2application.R;
import com.example.my2application.adapters.MyAdapter;
import com.example.my2application.interfasces.OnRecyclerViewItemClick;

import java.util.ArrayList;
import java.util.List;


public class FirstFragment extends Fragment {

    private RecyclerView recyclerView;
    private MyAdapter adapter;

    public static FirstFragment newInstance() {
        return new FirstFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_first, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findViews(view);
        initViews();
    }

    private void findViews(View view) {
        recyclerView = view.findViewById(R.id.recview);
    }

    private void initViews() {
        adapter = new MyAdapter(createFakeData(1000));
        adapter.setmClickListener((OnRecyclerViewItemClick) getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
    }

    private List<Integer> createFakeData(int count) {
        int i = 0;
        List<Integer> fakeData = new ArrayList<>();
        while (i < count) {
            fakeData.add(i);
            i++;
        }
        return fakeData;
    }
}
