package com.example.my3application.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.my3application.R;
import com.example.my3application.room.Contact;


public class ContactDisplayFragment extends Fragment {
    private final static String KEY = "key";

    private Contact contact;
    private TextView textViewContact;

    public static ContactDisplayFragment newInstance(Contact contact) {
        ContactDisplayFragment fragment = new ContactDisplayFragment();
        Bundle args = new Bundle();
        args.putParcelable(KEY, contact);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            contact = getArguments().getParcelable(KEY);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        textViewContact = view.findViewById(R.id.contact_display_textview);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contact_display, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        textViewContact.setText(contact.toString());
    }

}
