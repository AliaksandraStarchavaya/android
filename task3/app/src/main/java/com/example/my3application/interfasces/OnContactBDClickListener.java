package com.example.my3application.interfasces;

import com.example.my3application.room.Contact;

public interface OnContactBDClickListener {
    void onContactDbClick(Contact contact);
}
