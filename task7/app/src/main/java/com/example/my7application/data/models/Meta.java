package com.example.my7application.data.models;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Meta {
    @SerializedName("title")
    private String title;
    @SerializedName("image")
    private String image;
    @SerializedName("fields")
    private List<Field> fields;

    public List<Field> getFields() {
        return fields;
    }

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }

    @NonNull
    @Override
    public String toString() {
        return "Meta{" +
                "title='" + title + '\'' +
                ", image='" + image + '\'' +
                ", fields=" + fields +
                '}';
    }
}
