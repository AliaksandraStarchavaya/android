package com.example.my7application.di;


import com.example.my7application.App;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

@Component(modules = {MetaModule.class})
@Singleton
public interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder withApplication(App application);

        AppComponent build();
    }

    ViewModelSubComponent.Builder viewModelSubComponentBuilder();
}