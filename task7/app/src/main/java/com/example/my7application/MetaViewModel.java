package com.example.my7application;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.my7application.adapter.RecyclerViewAdapter;
import com.example.my7application.data.meta.repository.MetaRepository;
import com.example.my7application.data.models.Answer;
import com.example.my7application.data.models.Field;
import com.example.my7application.data.models.Meta;
import com.example.my7application.data.models.TypeField;
import com.example.my7application.data.models.rowModels.ListRowType;
import com.example.my7application.data.models.rowModels.NumericRowType;
import com.example.my7application.data.models.rowModels.RowType;
import com.example.my7application.data.models.rowModels.TextRowType;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MetaViewModel extends AndroidViewModel {
    @Inject
    MetaRepository metaRepository;

    private MutableLiveData<Meta> liveData;
    private MutableLiveData<Answer> answer;

    public MetaViewModel(@NonNull Application application) {
        super(application);
        ((App) application)
                .getViewModelSubComponent()
                .inject(this);
    }

    MutableLiveData<Meta> getData(View progressBar) {
        liveData = new MutableLiveData<>();
        createData(progressBar);
        return liveData;
    }

    MutableLiveData<Answer> getAnswer(View progressBar, JsonObject jsonObject) {
        answer = new MutableLiveData<>();
        createAnswer(progressBar, jsonObject);
        return answer;
    }

    @SuppressLint("CheckResult")
    private void createAnswer(View progressBar, JsonObject jsonObject) {
        progressBar.setVisibility(ProgressBar.VISIBLE);
        Single<Answer> metaSingle = metaRepository.sendReply(jsonObject);
        metaSingle
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        response -> {
                            answer.setValue(response);
                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                        },
                        error -> {
                            System.out.println(error.getMessage());
                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                        });
    }

    @SuppressLint("CheckResult")
    private void createData(View progressBar) {
        progressBar.setVisibility(ProgressBar.VISIBLE);
        Single<Meta> metaSingle = metaRepository.getMeta().map(MetaViewModel::createForm);
        metaSingle
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        response -> {
                            List<Field> fieldList = new ArrayList<>();
                            liveData.setValue(response);
                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                        },
                        error -> {
                            System.out.println(error.getMessage());
                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                        });
    }

    private static Meta createForm(Meta meta) {
        Activity currentActivity = ((App) App.getAppContext().getApplicationContext()).getCurrentActivity();
        List<Field> fieldList = new ArrayList<>();
        List<RowType> rowTypes = new ArrayList<>();
        for (Field field : meta.getFields()) {
            fieldList.add(field);
            if (field.getType() == TypeField.TEXT) {
                rowTypes.add(new TextRowType(field.getTitle(), field.getName()));
            }
            if (field.getType() == TypeField.NUMERIC) {
                rowTypes.add(new NumericRowType(field.getTitle(), field.getName()));
            }
            if (field.getType() == TypeField.LIST) {
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(App.getAppContext(), android.R.layout.simple_spinner_item, field.getValues().getValues());
                rowTypes.add(new ListRowType(field.getTitle(), field.getName(), adapter));
            }
        }

        currentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                RecyclerView recyclerView = (RecyclerView) currentActivity.findViewById(R.id.res);
                recyclerView.setLayoutManager(new LinearLayoutManager(App.getAppContext()));
                recyclerView.setAdapter(new RecyclerViewAdapter(rowTypes));
            }
        });
        return meta;
    }
}