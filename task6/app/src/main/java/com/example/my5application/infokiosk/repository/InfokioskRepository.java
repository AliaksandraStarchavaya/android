package com.example.my5application.infokiosk.repository;

import com.example.my5application.infokiosk.services.ServicesApiInfokiosk;
import com.example.my5application.mapper.MapperToPlaceBelarusbank;
import com.example.my5application.models.PlaceBelarusbank;
import com.example.my5application.repository.FindingBankRepository;

import java.util.List;

import dagger.Module;
import io.reactivex.Single;

@Module
public class InfokioskRepository implements FindingBankRepository {
    private ServicesApiInfokiosk servicesApi;

    public InfokioskRepository(ServicesApiInfokiosk servicesApi) {
        this.servicesApi = servicesApi;
    }

    @Override
    public Single<List<PlaceBelarusbank>> getPlaceBelarusbank(String city) {
        return servicesApi.getApiService().getInfokiosks(city)
                .map(MapperToPlaceBelarusbank::convertInfokioskToPlace);
    }
}
