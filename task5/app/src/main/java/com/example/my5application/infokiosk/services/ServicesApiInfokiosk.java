package com.example.my5application.infokiosk.services;

import com.example.my5application.infokiosk.services.interfaces.InfokioskApiService;

import retrofit2.Retrofit;

public class ServicesApiInfokiosk {
    private Retrofit retrofit;

    public ServicesApiInfokiosk() {
        this.retrofit = RetrofitServiceInfokiosk
                .getInstance()
                .getRetrofit();
    }

    public InfokioskApiService getApiService() {
        return retrofit.create(InfokioskApiService.class);
    }
}
