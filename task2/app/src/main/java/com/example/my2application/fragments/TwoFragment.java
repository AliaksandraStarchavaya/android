package com.example.my2application.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.my2application.MainActivity;
import com.example.my2application.R;


public class TwoFragment extends Fragment {

    private Integer text;
    private Button backBtn;

    public static TwoFragment newInstance(Integer text) {
        Bundle args = new Bundle();
        args.putInt(MainActivity.KEYTEXTRECVIEW, text);
        TwoFragment fragment = new TwoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        backBtn = view.findViewById(R.id.backBtn);
        backBtn.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("finish", true);
            startActivity(intent);
            // if (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0) {
            //     getActivity().getSupportFragmentManager().popBackStack();
            // }
        });

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            text = getArguments().getInt(MainActivity.KEYTEXTRECVIEW);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_two, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        TextView textView1 = getActivity().findViewById(R.id.text1TwoFragment);
        textView1.setText("Title " + text);
        TextView textView2 = getActivity().findViewById(R.id.text2TwoFragment);
        textView2.setText("Description  " + text);
    }
}
