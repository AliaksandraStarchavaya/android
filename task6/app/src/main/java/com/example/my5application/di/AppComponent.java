package com.example.my5application.di;

import com.example.my5application.App;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;


@Component(modules = {BankDivisionsModule.class, СashMachineModule.class, InfokioskModule.class})
@Singleton
public interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder withApplication(App application);

        AppComponent build();
    }

    ViewModelSubComponent.Builder viewModelSubComponentBuilder();
}