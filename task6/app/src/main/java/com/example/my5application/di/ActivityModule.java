package com.example.my5application.di;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.example.my5application.PlaceBelarusbankViewModel;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {

    @Provides
    public PlaceBelarusbankViewModel placeBelarusbankViewModel(AppCompatActivity activity) {

        return ViewModelProviders.of(activity).get(PlaceBelarusbankViewModel.class);
    }
}