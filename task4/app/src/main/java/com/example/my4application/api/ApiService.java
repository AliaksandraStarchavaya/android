package com.example.my4application.api;

import com.example.my4application.models.СashMachineModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    @GET("CashMachines")
    Call<List<СashMachineModel>> getCashMachine(@Query("city") String city);
}
