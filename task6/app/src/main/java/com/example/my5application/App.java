package com.example.my5application;

import android.app.Application;


import com.example.my5application.di.AppComponent;
import com.example.my5application.di.DaggerAppComponent;
import com.example.my5application.di.ViewModelSubComponent;


public class App extends Application {

    private AppComponent appComponent;
    private ViewModelSubComponent viewModelSubComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent
                .builder()
                .withApplication(this)
                .build();

        viewModelSubComponent = appComponent
                .viewModelSubComponentBuilder()
                .build();
    }

    public ViewModelSubComponent getViewModelSubComponent() {
        return viewModelSubComponent;
    }
}

