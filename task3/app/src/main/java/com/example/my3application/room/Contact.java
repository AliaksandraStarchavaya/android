package com.example.my3application.room;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Contact implements Parcelable {

    public Contact() {
    }

    public Contact(long id, String name, String phone, String email, String family) {
        this.id = id;
        this.name = name;
        this.family = family;
        this.phone = phone;
        this.email = email;
    }

    public static final Creator<Contact> CREATOR = new Creator<Contact>() {
        @Override
        public Contact createFromParcel(Parcel source) {
            long id = source.readLong();
            String name = source.readString();
            String family = source.readString();
            String phone = source.readString();
            String email = source.readString();
            return new Contact(id, name, phone, email, family);
        }

        @Override
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };
    @PrimaryKey(autoGenerate = true)
    public long id;

    @ColumnInfo(name = "name")
    public String name;
    @ColumnInfo(name = "family")
    public String family;

    @ColumnInfo(name = "phone_number")
    public String phone;

    @ColumnInfo(name = "email")
    public String email;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(phone);
        dest.writeString(email);
        dest.writeString(family);
    }

    @Override
    public String toString() {
        StringBuilder contact = new StringBuilder();
        if (name != null) {
            contact.append(name);
            contact.append("\n");
        }
        if (family != null) {
            contact.append(family);
            contact.append("\n");
        }
        if (phone != null) {
            contact.append(phone);
            contact.append("\n");
        }
        if (email != null) {
            contact.append(email);
            contact.append("\n");
        }
        return contact.toString();
    }
}
