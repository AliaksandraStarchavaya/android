package com.example.my3application.room;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ContactDaoContact {

    @Query("SELECT * FROM Contact")
    List<Contact> getAll();

    @Query("SELECT * FROM Contact WHERE phone_number = :number")
    Contact getByNumber(String number);

    @Query("SELECT * FROM Contact WHERE id = :id")
    Contact getById(long id);

    @Insert
    void insert(Contact employee);

    @Update
    void update(Contact employee);

    @Delete
    void delete(Contact employee);

}