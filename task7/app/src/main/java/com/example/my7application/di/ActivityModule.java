package com.example.my7application.di;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.example.my7application.MetaViewModel;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {

    @Provides
    public MetaViewModel metaViewModel(AppCompatActivity activity) {
        return ViewModelProviders.of(activity).get(MetaViewModel.class);
    }
}