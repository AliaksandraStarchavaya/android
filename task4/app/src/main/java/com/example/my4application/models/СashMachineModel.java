package com.example.my4application.models;

import com.google.gson.annotations.SerializedName;

public class СashMachineModel {
    @SerializedName("id")
    private int id;
    @SerializedName("area")
    private String area;
    @SerializedName("city_type")
    private String cityType;
    @SerializedName("city")
    private String city;
    @SerializedName("address_type")
    private String addressType;
    @SerializedName("address")
    private String address;
    @SerializedName("house")
    private String house;
    @SerializedName("install_place")
    private String installPlace;
    @SerializedName("work_time")
    private String workTime;
    @SerializedName("gps_x")
    private double gpsX;
    @SerializedName("gps_y")
    private double gpsY;
    @SerializedName("install_place_full")
    private String installPlaceFull;
    @SerializedName("work_time_full")
    private String workTimeFull;
    @SerializedName("ATM_type")
    private String ATMType;
    @SerializedName("ATM_error")
    private String ATMError;
    @SerializedName("currency")
    private String currency;
    @SerializedName("cash_in")
    private String cashIn;
    @SerializedName("ATM_printer")
    private String ATMPrinter;

    public int getId() {
        return id;
    }

    public String getArea() {
        return area;
    }

    public String getCityType() {
        return cityType;
    }

    public String getCity() {
        return city;
    }

    public String getAddressType() {
        return addressType;
    }

    public String getAddress() {
        return address;
    }

    public String getHouse() {
        return house;
    }

    public String getInstallPlace() {
        return installPlace;
    }

    public String getWorkTime() {
        return workTime;
    }

    public double getGpsX() {
        return gpsX;
    }

    public double getGpsY() {
        return gpsY;
    }

    public String getInstallPlaceFull() {
        return installPlaceFull;
    }

    public String getWorkTimeFull() {
        return workTimeFull;
    }

    public String getATMType() {
        return ATMType;
    }

    public String getATMError() {
        return ATMError;
    }

    public String getCurrency() {
        return currency;
    }

    public String getCashIn() {
        return cashIn;
    }

    public String getATMPrinter() {
        return ATMPrinter;
    }

    @Override
    public String toString() {
        return "СashMachineModel{" +
                "id=" + id +
                ", area='" + area + '\'' +
                ", cityType='" + cityType + '\'' +
                ", city='" + city + '\'' +
                ", addressType='" + addressType + '\'' +
                ", address='" + address + '\'' +
                ", house='" + house + '\'' +
                ", installPlace='" + installPlace + '\'' +
                ", workTime='" + workTime + '\'' +
                ", gpsX=" + gpsX +
                ", gpsY=" + gpsY +
                ", installPlaceFull='" + installPlaceFull + '\'' +
                ", workTimeFull='" + workTimeFull + '\'' +
                ", ATMType='" + ATMType + '\'' +
                ", ATMError='" + ATMError + '\'' +
                ", currency='" + currency + '\'' +
                ", cashIn='" + cashIn + '\'' +
                ", ATMPrinter='" + ATMPrinter + '\'' +
                '}';
    }
}
