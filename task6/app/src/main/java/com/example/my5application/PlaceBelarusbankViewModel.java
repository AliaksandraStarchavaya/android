package com.example.my5application;

import android.annotation.SuppressLint;
import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.example.my5application.bankDivisions.repository.BankDivisionsRepository;
import com.example.my5application.cashMachine.repository.СashMachineRepository;
import com.example.my5application.infokiosk.repository.InfokioskRepository;
import com.example.my5application.models.PlaceBelarusbank;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class PlaceBelarusbankViewModel extends AndroidViewModel {

    @Inject
    СashMachineRepository cashMachineRepository;
    @Inject
    InfokioskRepository infokioskRepository;
    @Inject
    BankDivisionsRepository bankDivisionsRepository;


    private MutableLiveData<List<PlaceBelarusbank>> liveData;

    public PlaceBelarusbankViewModel(Application application) {
        super(application);
        ((App) application)
                .getViewModelSubComponent()
                .inject(this);
    }

    MutableLiveData<List<PlaceBelarusbank>> getData() {
        if (liveData == null) {
            liveData = new MutableLiveData<>();
            createData();
        }
        return liveData;
    }

    private List<PlaceBelarusbank> sortPlace(List<PlaceBelarusbank> plases) {
        Collections.sort(plases, new Comparator<PlaceBelarusbank>() {
            @Override
            public int compare(PlaceBelarusbank lhs, PlaceBelarusbank rhs) {
                return Double.compare(lhs.getDistanceToPoint(), rhs.getDistanceToPoint());
            }
        });
        return plases;
    }

    private List<PlaceBelarusbank> findNearbyPoints(List<PlaceBelarusbank> plases) {
        return new ArrayList<>(plases.subList(0, 10));
    }

    private static List<PlaceBelarusbank> join(List<PlaceBelarusbank> l1, List<PlaceBelarusbank> l2, List<PlaceBelarusbank> l3) {
        List<PlaceBelarusbank> result = new ArrayList<>(l1);
        result.addAll(l1);
        result.addAll(l2);
        result.addAll(l3);
        return result;
    }

    @SuppressLint("CheckResult")
    private void createData() {
        Single<List<PlaceBelarusbank>> placesCashMachine = cashMachineRepository.getPlaceBelarusbank("Гомель");
        Single<List<PlaceBelarusbank>> placesInfokiosk = infokioskRepository.getPlaceBelarusbank("Гомель");
        Single<List<PlaceBelarusbank>> placesBankDivisions = bankDivisionsRepository.getPlaceBelarusbank("Гомель");
        Single<List<PlaceBelarusbank>> single = Single.zip(placesBankDivisions, placesCashMachine, placesInfokiosk,
                PlaceBelarusbankViewModel::join).map(p -> (List<PlaceBelarusbank>) new ArrayList<>(new HashSet<>(p))).map(this::sortPlace).map(this::findNearbyPoints);
        single
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        response -> {
                            liveData.setValue(response);
                        },
                        Throwable::getStackTrace);
    }


}