package com.example.my5application;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.example.my5application.bankDivisions.repository.BankDivisionsRepository;
import com.example.my5application.cashMachine.repository.СashMachineRepository;
import com.example.my5application.infokiosk.repository.InfokioskRepository;
import com.example.my5application.models.PlaceBelarusbank;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    List<PlaceBelarusbank> plases;
    public static double x = 52.425163;
    public static double y = 31.015039;
    private static final int PERMISSION_REQUEST_CODE = 2356;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        loadRates();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE) {
            getPermission();
        }
    }

    public void getPermission() {
        boolean permissionAccessCoarseLocationApproved =
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED;
        boolean permissionAccessFineLocationApproved =
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED;
        if (permissionAccessCoarseLocationApproved && permissionAccessFineLocationApproved) {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            if (mapFragment != null) {
                mapFragment.getMapAsync(this);
            }
        } else {
            ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(x, y), 15));
        googleMap.setMyLocationEnabled(true);
        for (PlaceBelarusbank plase : plases) {
            LatLng coordinates = new LatLng(plase.getGpsX(), plase.getGpsY());
            googleMap.addMarker(new MarkerOptions().position(coordinates).title(plase.getAddress() + plase.getHouse()));

        }
    }

    private List<PlaceBelarusbank> sortPlace(List<PlaceBelarusbank> plases) {
        Collections.sort(plases, new Comparator<PlaceBelarusbank>() {
            @Override
            public int compare(PlaceBelarusbank lhs, PlaceBelarusbank rhs) {
                return Double.compare(lhs.getDistanceToPoint(), rhs.getDistanceToPoint());
            }
        });
        return plases;
    }

    private List<PlaceBelarusbank> findNearbyPoints(List<PlaceBelarusbank> plases) {
        return new ArrayList<>(plases.subList(0, 10));
    }

    private static List<PlaceBelarusbank> join(List<PlaceBelarusbank> l1, List<PlaceBelarusbank> l2, List<PlaceBelarusbank> l3) {
        List<PlaceBelarusbank> result = new ArrayList<>(l1);
        result.addAll(l1);
        result.addAll(l2);
        result.addAll(l3);
        return result;
    }

    @SuppressLint("CheckResult")
    private void loadRates() {
        Single<List<PlaceBelarusbank>> placesCashMachine = new СashMachineRepository().getPlaceBelarusbank("Гомель");
        Single<List<PlaceBelarusbank>> placesInfokiosk = new InfokioskRepository().getPlaceBelarusbank("Гомель");
        Single<List<PlaceBelarusbank>> placesBankDivisions = new BankDivisionsRepository().getPlaceBelarusbank("Гомель");
        Single<List<PlaceBelarusbank>> single = Single.zip(placesBankDivisions, placesCashMachine, placesInfokiosk,
                MapsActivity::join).map(p -> (List<PlaceBelarusbank>)new ArrayList<>(new HashSet<>(p))).map(this::sortPlace).map(this::findNearbyPoints);
        single
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        response -> {
                            plases = response;
                            getPermission();
                        },
                        Throwable::getStackTrace);
    }
}
