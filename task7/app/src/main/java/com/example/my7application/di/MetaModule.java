package com.example.my7application.di;


import com.example.my7application.data.meta.repository.MetaRepository;
import com.example.my7application.data.meta.services.ServicesApiMeta;

import dagger.Module;
import dagger.Provides;

@Module
public class MetaModule {
    @Provides
    MetaRepository provideMetaRepository(ServicesApiMeta servicesApi) {
        return new MetaRepository(servicesApi);
    }

    @Provides
    ServicesApiMeta provideServicesApiMeta() {
        return new ServicesApiMeta();
    }
}
