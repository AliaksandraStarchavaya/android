package com.example.my7application.data.meta.services;

import com.example.my7application.data.models.Answer;
import com.example.my7application.data.models.Meta;
import com.google.gson.JsonObject;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface MetaApiService {

    @GET("meta")
    Single<Meta> getMeta();

    @POST("data")
    Single<Answer> sendReply(@Body JsonObject bean);
}
