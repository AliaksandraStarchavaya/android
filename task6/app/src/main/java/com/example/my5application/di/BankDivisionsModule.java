package com.example.my5application.di;

import com.example.my5application.bankDivisions.repository.BankDivisionsRepository;
import com.example.my5application.bankDivisions.services.ServicesApiBankDivisions;

import dagger.Module;
import dagger.Provides;

@Module
public class BankDivisionsModule {
    @Provides
    BankDivisionsRepository provideBankDivisionsRepository(ServicesApiBankDivisions servicesApi) {
        return new BankDivisionsRepository(servicesApi);
    }
    @Provides
    ServicesApiBankDivisions provideServicesApiBankDivisions() {
        return new ServicesApiBankDivisions();
    }
}
