package com.example.my7application;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.example.my7application.di.AppComponent;
import com.example.my7application.di.DaggerAppComponent;
import com.example.my7application.di.ViewModelSubComponent;

public class App extends Application {

    private static Context context;
    private ViewModelSubComponent viewModelSubComponent;
    private Activity mCurrentActivity = null;

    public Activity getCurrentActivity() {
        return mCurrentActivity;
    }

    public void setCurrentActivity(Activity mCurrentActivity) {
        this.mCurrentActivity = mCurrentActivity;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        App.context = getApplicationContext();
        AppComponent appComponent = DaggerAppComponent
                .builder()
                .withApplication(this)
                .build();

        viewModelSubComponent = appComponent
                .viewModelSubComponentBuilder()
                .build();
    }

    public ViewModelSubComponent getViewModelSubComponent() {
        return viewModelSubComponent;
    }

    public static Context getAppContext() {
        return App.context;
    }
}

