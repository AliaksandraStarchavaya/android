package com.example.my7application.data.models;

public enum TypeField {
    TEXT,
    NUMERIC,
    LIST
}
