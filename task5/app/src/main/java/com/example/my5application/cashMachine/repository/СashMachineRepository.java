package com.example.my5application.cashMachine.repository;

import com.example.my5application.cashMachine.services.ServicesApiСashMachine;
import com.example.my5application.mapper.MapperToPlaceBelarusbank;
import com.example.my5application.models.PlaceBelarusbank;
import com.example.my5application.repository.FindingBankRepository;

import java.util.List;

import io.reactivex.Single;

public class СashMachineRepository implements FindingBankRepository {
    private ServicesApiСashMachine servicesApi = new ServicesApiСashMachine();

    @Override
    public Single<List<PlaceBelarusbank>> getPlaceBelarusbank(String city) {
        Single<List<PlaceBelarusbank>> map = servicesApi.getApiService().getCashMachine(city)
                .map(MapperToPlaceBelarusbank::convertCashMachineToPlace);
        return map;
    }
}
