package com.example.my7application;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.bumptech.glide.Glide;
import com.example.my7application.adapter.RecyclerViewAdapter;;
import com.example.my7application.data.models.Answer;
import com.example.my7application.data.models.FieldsForm;
import com.example.my7application.data.models.Meta;
import com.example.my7application.fragments.AnswerDisplayFragment;
import com.google.gson.JsonObject;

public class MainActivity extends AppCompatActivity {
    protected App mMyApp;
    LiveData<Meta> liveData;
    LiveData<Answer> answer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mMyApp = (App) this.getApplicationContext();
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(ProgressBar.VISIBLE);

        MetaViewModel model = ViewModelProviders.of(this).get(MetaViewModel.class);
        liveData = model.getData(progressBar);

        liveData.observe(this, new Observer<Meta>() {
            @Override
            public void onChanged(Meta meta) {
                uploadPicture(meta.getImage());
                setTitle(meta.getTitle());
            }
        });

        Button showDataBtn = (Button) findViewById(R.id.button3);
        JsonObject form = new JsonObject();
        JsonObject param = new JsonObject();
        showDataBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (FieldsForm form : RecyclerViewAdapter.listView) {
                    if (form.getView().getClass() == AppCompatEditText.class) {
                        EditText editText = (EditText) form.getView();
                        param.addProperty(form.getName(), editText.getText().toString());
                    }
                    if (form.getView().getClass() == AppCompatSpinner.class) {
                        Spinner spinner = (Spinner) form.getView();
                        param.addProperty(form.getName(), spinner.getSelectedItem().toString());
                    }
                }
                form.add("form", param);
                sendRequest(progressBar, form);
            }
        });
    }

    private void sendRequest(View view, JsonObject obj) {
        MetaViewModel modelAnswer = ViewModelProviders.of(this).get(MetaViewModel.class);
        answer = modelAnswer.getAnswer(view, obj);
        answer.observe(this, new Observer<Answer>() {
            @Override
            public void onChanged(Answer answer) {
                AnswerDisplayFragment dialog = new AnswerDisplayFragment();
                Bundle args = new Bundle();
                args.putString("result", answer.getResult());
                dialog.setArguments(args);
                dialog.show(getSupportFragmentManager(), "custom");
            }
        });
    }

    private void uploadPicture(String url) {
        ImageView imageView = findViewById(R.id.image);
        Glide
                .with(this)
                .load(url)
                .into(imageView);
    }

    protected void onResume() {
        super.onResume();
        mMyApp.setCurrentActivity(this);
    }

    protected void onPause() {
        clearReferences();
        super.onPause();
    }

    protected void onDestroy() {
        clearReferences();
        super.onDestroy();
    }

    private void clearReferences() {
        Activity currActivity = mMyApp.getCurrentActivity();
        if (this.equals(currActivity))
            mMyApp.setCurrentActivity(null);
    }
}
