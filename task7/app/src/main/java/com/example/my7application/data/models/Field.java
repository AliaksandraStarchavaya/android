package com.example.my7application.data.models;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class Field {
    @SerializedName("title")
    private String title;
    @SerializedName("name")
    private String name;
    @SerializedName("type")
    private TypeField type;
    @SerializedName("values")
    private Values values;

    public String getTitle() {
        return title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TypeField getType() {
        return type;
    }

    public Values getValues() {
        return values;
    }

    @NonNull
    @Override
    public String toString() {
        return "Field{" +
                "title='" + title + '\'' +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", values=" + values +
                '}';
    }
}