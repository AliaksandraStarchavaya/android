package com.example.my5application.models;

import com.example.my5application.MapsActivity;

import java.io.Serializable;

public class PlaceBelarusbank implements Serializable {
    private String address;
    private String house;
    private double gpsX;
    private double gpsY;
    private double distanceToPoint;

    public double getDistanceToPoint() {
        return distanceToPoint;
    }

    public PlaceBelarusbank(String address, String house, double gpsX, double gpsY) {
        this.address = address;
        this.house = house;
        this.gpsX = gpsX;
        this.gpsY = gpsY;
        this.distanceToPoint = Math.sqrt(Math.pow(gpsX - MapsActivity.x, 2) + Math.pow(gpsY - MapsActivity.y, 2));
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public double getGpsX() {
        return gpsX;
    }

    public void setGpsX(double gpsX) {
        this.gpsX = gpsX;
    }

    public double getGpsY() {
        return gpsY;
    }

    public void setGpsY(double gpsY) {
        this.gpsY = gpsY;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlaceBelarusbank that = (PlaceBelarusbank) o;
        return Double.compare(that.gpsX, gpsX) == 0 &&
                Double.compare(that.gpsY, gpsY) == 0 &&
                Double.compare(that.distanceToPoint, distanceToPoint) == 0 &&
                (address.equals(that.address) &&
                        house.equals(that.house));
    }

    @Override
    public int hashCode() {
        return (int) Math.round(address.hashCode() + house.hashCode() + gpsX + gpsY + distanceToPoint);
    }

    @Override
    public String toString() {
        return "PlaceBelarusbank{" +
                "address='" + address + '\'' +
                ", house='" + house + '\'' +
                ", gpsX=" + gpsX +
                ", gpsY=" + gpsY +
                ", distanceToPoint=" + distanceToPoint +
                '}';
    }
}
