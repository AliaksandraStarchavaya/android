package com.example.my7application.data.models.rowModels;

import androidx.recyclerview.widget.RecyclerView;

public interface RowType {
    int TEXT_ROW_TYPE = 0;
    int NUMERIC_ROW_TYPE = 1;
    int LIST_ROW_TYPE = 2;

    int getItemViewType();

    void onBindViewHolder(RecyclerView.ViewHolder viewHolder);
}
