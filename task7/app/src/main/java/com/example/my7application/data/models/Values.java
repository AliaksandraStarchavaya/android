package com.example.my7application.data.models;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Values {
    @SerializedName("none")
    private String none;
    @SerializedName("v1")
    private String v1;
    @SerializedName("v2")
    private String v2;
    @SerializedName("v3")
    private String v3;

    public Values(String none, String v1, String v2, String v3) {
        this.none = none;
        this.v1 = v1;
        this.v2 = v2;
        this.v3 = v3;
    }

    public List<String> getValues() {
        List<String> stringList = new ArrayList<>();
        stringList.add(none);
        stringList.add(v1);
        stringList.add(v2);
        stringList.add(v3);
        return stringList;
    }

    @NonNull
    @Override
    public String toString() {
        return "Values{" +
                "none='" + none + '\'' +
                ", v1='" + v1 + '\'' +
                ", v2='" + v2 + '\'' +
                ", v3='" + v3 + '\'' +
                '}';
    }
}
