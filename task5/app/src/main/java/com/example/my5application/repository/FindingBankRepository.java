package com.example.my5application.repository;

import com.example.my5application.models.PlaceBelarusbank;

import java.util.List;

import io.reactivex.Single;

public interface FindingBankRepository {
    Single<List<PlaceBelarusbank>> getPlaceBelarusbank(String city);
}
