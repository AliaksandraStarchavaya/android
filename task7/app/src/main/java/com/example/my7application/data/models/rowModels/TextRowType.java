package com.example.my7application.data.models.rowModels;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.my7application.adapter.RecyclerViewAdapter;
import com.example.my7application.data.models.FieldsForm;
import com.example.my7application.factory.ViewHolderFactory;

public class TextRowType implements RowType {

    private String tittle;
    private String name;

    public TextRowType(String tittle, String name) {
        this.tittle = tittle;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getItemViewType() {
        return RowType.TEXT_ROW_TYPE;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder) {
        ViewHolderFactory.TextViewHolder textViewHolder = (ViewHolderFactory.TextViewHolder) viewHolder;
        textViewHolder.textView.setText(tittle);
        RecyclerViewAdapter.listView.add(new FieldsForm(name, textViewHolder.editText));
    }

    @NonNull
    @Override
    public String toString() {
        return "TextRowType{" +
                "tittle='" + tittle + '\'' +
                '}';
    }
}
