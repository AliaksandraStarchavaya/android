package com.example.my5application.bankDivisions.repository;

import com.example.my5application.bankDivisions.services.ServicesApiBankDivisions;
import com.example.my5application.mapper.MapperToPlaceBelarusbank;
import com.example.my5application.models.PlaceBelarusbank;
import com.example.my5application.repository.FindingBankRepository;

import java.util.List;

import io.reactivex.Single;

public class BankDivisionsRepository implements FindingBankRepository {
    private ServicesApiBankDivisions servicesApi;

    public BankDivisionsRepository(ServicesApiBankDivisions servicesApi) {
        this.servicesApi = servicesApi;
    }

    @Override
    public Single<List<PlaceBelarusbank>> getPlaceBelarusbank(String city) {
        Single<List<PlaceBelarusbank>> map = servicesApi.getApiService().getBankDivisions(city)
                .map(MapperToPlaceBelarusbank::convertBankDivisionsToPlace);
        return map;
    }
}
