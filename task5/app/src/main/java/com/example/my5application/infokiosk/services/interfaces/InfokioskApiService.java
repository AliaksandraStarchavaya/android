package com.example.my5application.infokiosk.services.interfaces;

import com.example.my5application.models.InfokioskModel;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface InfokioskApiService {

    @GET("Infokiosks")
    Single<List<InfokioskModel>> getInfokiosks(@Query("city") String city);
}
