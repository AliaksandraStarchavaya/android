package com.example.my5application.models;

import com.google.gson.annotations.SerializedName;

public class InfokioskModel {
    @SerializedName("area")
    private String are;
    @SerializedName("city_type")
    private String cityType;
    @SerializedName("city")
    private String city;
    @SerializedName("address_type")
    private String addressType;
    @SerializedName("address")
    private String address;
    @SerializedName("house")
    private String house;
    @SerializedName("install_place")
    private String installPlace;
    @SerializedName("location_name_desc")
    private String locationNameDesc;
    @SerializedName("work_time")
    private String workTime;
    @SerializedName("time_long")
    private String timeLong;
    @SerializedName("gps_x")
    private double gpsX;
    @SerializedName("gps_y")
    private double gpsY;
    @SerializedName("currency")
    private String currency;
    @SerializedName("inf_type")
    private String infType;
    @SerializedName("cash_in_exist")
    private String cashInExist;
    @SerializedName("cash_in")
    private String cashIn;
    @SerializedName("type_cash_in")
    private String typeCashIn;
    @SerializedName("inf_printer")
    private String infPrinter;
    @SerializedName("popolnenie_platej")
    private String popolneniePlatej;
    @SerializedName("region_platej")
    private String regionPlatej;
    @SerializedName("inf_status")
    private String infStatus;

    public String getAre() {
        return are;
    }

    public void setAre(String are) {
        this.are = are;
    }

    public String getCityType() {
        return cityType;
    }

    public void setCityType(String cityType) {
        this.cityType = cityType;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getInstallPlace() {
        return installPlace;
    }

    public void setInstallPlace(String installPlace) {
        this.installPlace = installPlace;
    }

    public String getLocationNameDesc() {
        return locationNameDesc;
    }

    public void setLocationNameDesc(String locationNameDesc) {
        this.locationNameDesc = locationNameDesc;
    }

    public String getWorkTime() {
        return workTime;
    }

    public void setWorkTime(String workTime) {
        this.workTime = workTime;
    }

    public String getTimeLong() {
        return timeLong;
    }

    public void setTimeLong(String timeLong) {
        this.timeLong = timeLong;
    }

    public double getGpsX() {
        return gpsX;
    }

    public void setGpsX(double gpsX) {
        this.gpsX = gpsX;
    }

    public double getGpsY() {
        return gpsY;
    }

    public void setGpsY(double gpsY) {
        this.gpsY = gpsY;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getInfType() {
        return infType;
    }

    public void setInfType(String infType) {
        this.infType = infType;
    }

    public String getCashInExist() {
        return cashInExist;
    }

    public void setCashInExist(String cashInExist) {
        this.cashInExist = cashInExist;
    }

    public String getCashIn() {
        return cashIn;
    }

    public void setCashIn(String cashIn) {
        this.cashIn = cashIn;
    }

    public String getTypeCashIn() {
        return typeCashIn;
    }

    public void setTypeCashIn(String typeCashIn) {
        this.typeCashIn = typeCashIn;
    }

    public String getInfPrinter() {
        return infPrinter;
    }

    public void setInfPrinter(String infPrinter) {
        this.infPrinter = infPrinter;
    }

    public String getPopolneniePlatej() {
        return popolneniePlatej;
    }

    public void setPopolneniePlatej(String popolneniePlatej) {
        this.popolneniePlatej = popolneniePlatej;
    }

    public String getRegionPlatej() {
        return regionPlatej;
    }

    public void setRegionPlatej(String regionPlatej) {
        this.regionPlatej = regionPlatej;
    }

    public String getInfStatus() {
        return infStatus;
    }

    public void setInfStatus(String infStatus) {
        this.infStatus = infStatus;
    }


    @Override
    public String toString() {
        return "Infokiosk{" +
                "are='" + are + '\'' +
                ", cityType='" + cityType + '\'' +
                ", city='" + city + '\'' +
                ", addressType='" + addressType + '\'' +
                ", address='" + address + '\'' +
                ", house='" + house + '\'' +
                ", installPlace='" + installPlace + '\'' +
                ", locationNameDesc='" + locationNameDesc + '\'' +
                ", workTime='" + workTime + '\'' +
                ", timeLong='" + timeLong + '\'' +
                ", gpsX=" + gpsX +
                ", gpsY=" + gpsY +
                ", currency='" + currency + '\'' +
                ", infType='" + infType + '\'' +
                ", cashInExist='" + cashInExist + '\'' +
                ", cashIn='" + cashIn + '\'' +
                ", typeCashIn='" + typeCashIn + '\'' +
                ", infPrinter='" + infPrinter + '\'' +
                ", popolneniePlatej='" + popolneniePlatej + '\'' +
                ", regionPlatej='" + regionPlatej + '\'' +
                ", infStatus='" + infStatus + '\'' +
                '}';
    }
}
