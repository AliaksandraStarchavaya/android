package com.example.my5application.di;

import com.example.my5application.PlaceBelarusbankViewModel;

import dagger.Subcomponent;

@Subcomponent
public interface ViewModelSubComponent {

    @Subcomponent.Builder
    interface Builder {
        ViewModelSubComponent build();
    }

    void inject(PlaceBelarusbankViewModel placeBelarusbankViewModel);
}
