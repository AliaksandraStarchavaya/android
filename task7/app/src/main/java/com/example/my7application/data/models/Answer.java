package com.example.my7application.data.models;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class Answer {
    @SerializedName("result")
    private String result;

    public Answer(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    @NonNull
    @Override
    public String toString() {
        return "Answer{" +
                "result='" + result + '\'' +
                '}';
    }
}
