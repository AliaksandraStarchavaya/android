package com.example.my7application.data.models.rowModels;

import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.my7application.adapter.RecyclerViewAdapter;
import com.example.my7application.data.models.FieldsForm;
import com.example.my7application.factory.ViewHolderFactory;

public class ListRowType implements RowType {
    private String tittle;
    private String name;
    private ArrayAdapter<String> adapter;

    public ListRowType(String tittle, String name, ArrayAdapter<String> arrayAdapter) {
        this.tittle = tittle;
        this.adapter = arrayAdapter;
        this.name = name;
    }

    @Override
    public int getItemViewType() {
        return RowType.LIST_ROW_TYPE;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder) {
        ViewHolderFactory.ListViewHolder listViewHolder = (ViewHolderFactory.ListViewHolder) viewHolder;
        listViewHolder.listView.setText(tittle);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        listViewHolder.editList.setAdapter(adapter);
        RecyclerViewAdapter.listView.add(new FieldsForm(name, listViewHolder.editList));
    }

    @NonNull
    @Override
    public String toString() {
        return "ListRowType{" +
                "tittle='" + tittle + '\'' +
                ", adapter=" + adapter +
                '}';
    }
}