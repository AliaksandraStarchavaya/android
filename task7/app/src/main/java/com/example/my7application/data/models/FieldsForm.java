package com.example.my7application.data.models;

import android.view.View;

import androidx.annotation.NonNull;

public class FieldsForm {
    private String name;
    private View view;

    public FieldsForm(String name, View view) {
        this.name = name;
        this.view = view;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public View getView() {
        return view;
    }

    @NonNull
    @Override
    public String toString() {
        return "Answer{" +
                "name='" + name + '\'' +
                ", view=" + view +
                '}';
    }
}
