package com.example.my5application.bankDivisions.services;

import com.google.gson.GsonBuilder;
import com.squareup.picasso.BuildConfig;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitServiceBankDivisions {
    private static RetrofitServiceBankDivisions retrofitServiceInstance;
    private static final String BASE_URL = "https://belarusbank.by/api/filials_info/";
    private Retrofit mRetrofit;

    private RetrofitServiceBankDivisions() {
        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                        .create()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(createOkHttpClient())
                .build();
    }

    private OkHttpClient createOkHttpClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(getLoggingInterceptor())
                .build();
    }

    private HttpLoggingInterceptor getLoggingInterceptor() {
        if (BuildConfig.DEBUG) {
            return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE);
        }
    }

    public static RetrofitServiceBankDivisions getInstance() {
        if (retrofitServiceInstance == null) {
            retrofitServiceInstance = new RetrofitServiceBankDivisions();
        }
        return retrofitServiceInstance;
    }

    public Retrofit getRetrofit() {
        return mRetrofit;
    }
}
