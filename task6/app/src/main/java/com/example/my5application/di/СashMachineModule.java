package com.example.my5application.di;

import com.example.my5application.cashMachine.repository.СashMachineRepository;
import com.example.my5application.cashMachine.services.ServicesApiСashMachine;

import dagger.Module;
import dagger.Provides;

@Module
public class СashMachineModule {
    @Provides
    СashMachineRepository provideСashMachineRepository(ServicesApiСashMachine servicesApi) {
        return new СashMachineRepository(servicesApi);
    }

    @Provides
    ServicesApiСashMachine provideServicesApiСashMachine() {
        return new ServicesApiСashMachine();
    }
}
