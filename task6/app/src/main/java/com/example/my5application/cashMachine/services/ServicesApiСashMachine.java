package com.example.my5application.cashMachine.services;

import com.example.my5application.cashMachine.services.interfaces.СashMachineApiService;

import retrofit2.Retrofit;

public class ServicesApiСashMachine {
    private Retrofit retrofit;

    public ServicesApiСashMachine() {
        this.retrofit = RetrofitServiceСashMachine
                .getInstance()
                .getRetrofit();
    }

    public СashMachineApiService getApiService() {
        return retrofit.create(СashMachineApiService.class);
    }
}
