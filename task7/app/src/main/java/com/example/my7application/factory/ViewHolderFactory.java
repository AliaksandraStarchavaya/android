package com.example.my7application.factory;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.my7application.R;
import com.example.my7application.data.models.rowModels.RowType;

public class ViewHolderFactory {
    public static class TextViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        public EditText editText;

        TextViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.text);
            editText = itemView.findViewById(R.id.editText);
        }
    }

    public static class NumericViewHolder extends RecyclerView.ViewHolder {
        public TextView numericView;
        public EditText editNumeric;

        NumericViewHolder(@NonNull View itemView) {
            super(itemView);
            numericView = itemView.findViewById(R.id.numeric);
            editNumeric = itemView.findViewById(R.id.editNumeric);
        }
    }

    public static class ListViewHolder extends RecyclerView.ViewHolder {
        public TextView listView;
        public Spinner editList;

        ListViewHolder(@NonNull View itemView) {
            super(itemView);
            listView = itemView.findViewById(R.id.list);
            editList = itemView.findViewById(R.id.listEdit);
        }
    }

    public static RecyclerView.ViewHolder create(ViewGroup parent, int viewType) {
        switch (viewType) {
            case RowType.TEXT_ROW_TYPE:
                View textTypeView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_text_layout, parent, false);
                return new ViewHolderFactory.TextViewHolder(textTypeView);

            case RowType.NUMERIC_ROW_TYPE:
                View numericTypeView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_numeric_layout, parent, false);
                return new ViewHolderFactory.NumericViewHolder(numericTypeView);

            case RowType.LIST_ROW_TYPE:
                View listTypeView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_list_layout, parent, false);
                return new ViewHolderFactory.ListViewHolder(listTypeView);

            default:
                return null;
        }
    }
}
