package com.example.my5application.infokiosk.services;

import com.google.gson.GsonBuilder;
import com.squareup.picasso.BuildConfig;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitServiceInfokiosk {
    private static RetrofitServiceInfokiosk retrofitServiceInstance;
    private static final String BASE_URL = "https://belarusbank.by/api/infobox/";
    private Retrofit mRetrofit;

    private RetrofitServiceInfokiosk() {
        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                        .create()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(createOkHttpClient())
                .build();
    }

    private OkHttpClient createOkHttpClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(getLoggingInterceptor())
                .build();
    }

    private HttpLoggingInterceptor getLoggingInterceptor() {
        if (BuildConfig.DEBUG) {
            return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE);
        }
    }

    public static RetrofitServiceInfokiosk getInstance() {
        if (retrofitServiceInstance == null) {
            retrofitServiceInstance = new RetrofitServiceInfokiosk();
        }
        return retrofitServiceInstance;
    }

    public Retrofit getRetrofit() {
        return mRetrofit;
    }
}
