package com.example.my5application.di;

import com.example.my5application.infokiosk.repository.InfokioskRepository;
import com.example.my5application.infokiosk.services.ServicesApiInfokiosk;

import dagger.Module;
import dagger.Provides;

@Module
public class InfokioskModule {
    @Provides
    InfokioskRepository provideInfokioskRepository(ServicesApiInfokiosk servicesApiInfokiosk) {
        return new InfokioskRepository(servicesApiInfokiosk);
    }

    @Provides
    ServicesApiInfokiosk provideServicesApiInfokiosk() {
        return new ServicesApiInfokiosk();
    }
}
